# These templates have been reviewed by the debian-l10n-english
# team
#
# If modifications/additions/rewording are needed, please ask
# debian-l10n-english@lists.debian.org for advice.
#
# Even minor modifications require translation updates and such
# changes should be coordinated with translators and reviewers.

Template: localepurge/nopurge
Type: multiselect
Choices: ${locales}
_Description: Locale files to keep on this system:
 The localepurge package will remove all locale files from the system
 except those that you select here.
 .
 When selecting the locale corresponding to your language and country
 code (such as "de_DE", "de_CH", "it_IT", etc.) it is recommended to
 choose the two-character entry ("de", "it", etc.) as well.
 .
 Entries from /etc/locale.gen will be preselected
 if no prior configuration has been successfully completed.

Template: localepurge/use-dpkg-feature
Type: boolean
Default: true
_Description: Use dpkg --path-exclude?
 dpkg supports --path-exclude and --path-include options to filter files
 from packages being installed.
 .
 Please see /usr/share/doc/localepurge/README.dpkg-path for more
 information about this feature. It can be enabled (or disabled)
 later by running "dpkg-reconfigure localepurge".
 .
 This option will become active for packages
 unpacked after localepurge has been (re)configured. Packages
 installed or upgraded together with localepurge may (or may not) be
 subject to the previous configuration of localepurge.

Template: localepurge/none_selected
Type: boolean
Default: false
_Description: Really remove all locales?
 No locale has been chosen for being kept. This means that all locales will be
 removed from this system. Please confirm whether this is really your
 intent.

Template: localepurge/remove_no
Type: note
_Description: No localepurge action until the package is configured
 The localepurge package will not be useful until it has been successfully
 configured using the command "dpkg-reconfigure localepurge". The
 configured entries from /etc/locale.gen of the locales package will then
 be automatically preselected.

Template: localepurge/mandelete
Type: boolean
Default: true
_Description: Also delete localized man pages?
 Based on the same locale information you chose, localepurge can also
 delete localized man pages.

Template: localepurge/dontbothernew
Type: boolean
Default: false
_Description: Inform about new locales?
 If you choose this option, you will be given the opportunity
 to decide whether to keep or delete newly introduced locales.
 .
 If you don't choose it, newly introduced locales will be
 automatically dropped from the system.

Template: localepurge/showfreedspace
Type: boolean
Default: true
_Description: Display freed disk space?
 The localepurge program can display the disk space freed by each
 operation, and show a final summary of saved disk space.

Template: localepurge/quickndirtycalc
Type: boolean
Default: true
_Description: Accurate disk space calculation?
 There are two ways available to calculate freed disk space. One is
 much faster than the other but far less accurate if other changes occur
 on the file system during disk space calculation. The other one is more
 accurate but slower.

Template: localepurge/verbose
Type: boolean
Default: false
_Description: Display verbose output?
 The localepurge program can be configured to explicitly show which
 locale files it deletes. This may cause a lot of screen output.
